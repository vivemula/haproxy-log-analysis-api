<?php
//Convert file contents read from storage into objects for further use 
//Author: Vinay

ini_set('memory_limit',-1);

include "settings.php";
require_once("FileReader.php");
require_once("Log.php");

$DEBUGGING = false;

class LogReader{
	private $LogObjs;
	private $regex;	
	private $regexfile="../data/regex.txt";
	private $logfile="../data/haproxy.log";

	function __construct(){
	}

	private function setRegex(){
		$fr=new FileReader();
		$this->regex=$fr->getFileContents(WEB_ROOT.$this->regexfile);
	}

	public function getRegex(){
		return ($this->regex);
	}

	//read log and save in array
	public function readLogs(){
		
		$starttime=microtime(true);
		$LogObjs = array();		

		$file=fopen(WEB_ROOT.$this->logfile,"r");
		if($file==false){
                echo("Unable to read file");
                exit;
        }

		$count=0;	
		if($file){	
			while (($line = fgets($file)) !== false) {
				$linearray=explode(" ",$line);	

				$LogObj= new Log($linearray);
				$LogObjs[$LogObj->getAcceptDate()]=$LogObj;				
	
				++$count;
			}
			
			fclose($file);	
		}

		$endtime=microtime(true);
		
		if($DEBUGGING)
			echo "Time to create $count objects: ".($endtime-$starttime)." us\n";

		return $LogObjs;
	}
}
?>
