<?php
//Regular Expression functions 
//Return matched strings based on input string and pattern
//Author: Vinay

class RegexMatch{

	public static function getMatches($regex, $string){
		if(preg_match("~$regex~",$string,$match)) {
	
			return $match;
		}

		return null;
	}

}
