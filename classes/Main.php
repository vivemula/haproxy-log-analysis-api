<?php
//Starting point of execution
//Loads log data into objects
//Author: Vinay

include "settings.php";
require_once("LogReader.php");
require_once("Log.php");

class Main{
	private static $LogObjs=array();

	function __construct(){
		if(count($this->LogObjs)==0) {
			$lr = new LogReader();
			$this->LogObjs=$lr->readLogs();
		}	
	}

	public static function getLogObjSize(){
		return count(self::$LogObjs);
	}

	public function isLogPresent($starttime){
		if(isset($this->LogObjs[$starttime])) 
			return true;
		else	
			return false;
	}

	public function getLogObj($starttime){

		if($this->isLogPresent($starttime))
			return $this->LogObjs[$starttime];
		else
			return false;	
	}
}
