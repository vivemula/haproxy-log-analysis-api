<?php

//Class to read file contents from storage and 
//return the contents to other classes
//Author: Vinay

class FileReader{
	private $filecontents;	

	function __construct(){
	}		

	public function getFileContents($filename){

		$file=fopen($filename,"r");
	 	if($file==false){
			echo("Unable to read file");
			exit;
		}

		$filesize=filesize($file);
		$filecontents=fread($file,$filesize);

		fclose($file);

		return $filecontents;
	}
	
}
?>
