<?php
//Log object corresponding to each line written to log file
//Author: Vinay

require_once("RegexMatch.php");

//Class to create objects corresponding to each log entry

class Log{
	private $process_name;
	private $pid;
	private $client_ip;
	private $client_port;
	private $accept_date;
	private $frontend_name;
	private $backend_name;
	private $server_name;
	private $tq; 
	private $tw;
	private $tc;
	private $tr;
	private $tt;

	private $status_code;
	private $bytes_read;
	private $captured_request_cookie;
	private $captured_response_cookie;
	private $termination_state;
	private $actconn;
	private $feconn;
	private $beconn;
	private $srv_conn;
	private $retries;

	private $srv_queue;
	private $backend_queue;
	private $captured_request_headers;
	private $captured_response_headers;
	private $http_request;
	
	function __construct($linearray){
		$this->createLogFromLine($linearray);
	}

	function __destruct(){}
	
	//Create Log object from log entry
	public function createLogFromLine($linearray){
		list(,$this->process_name,$this->pid)=RegexMatch::getMatches("(.*)\[(\d*)\]",$linearray[5]);			
		list(,$this->client_ip,$this->client_port)=RegexMatch::getMatches("([0-9\.]*):(\d*)",$linearray[6]);			
		list(,$this->accept_date)=RegexMatch::getMatches("\[(.*)\]",$linearray[7]);			
		$this->setFrontendName($linearray[8]);
		list(,$this->backend_name,$this->server_name)=RegexMatch::getMatches("(.*)/(.*)",$linearray[9]);			
		list(,$this->tq,$this->tw,$this->tc,$this->tr,$this->tt)=RegexMatch::getMatches("([\d-])*\/([\d-]*)\/([\d-]*)\/([\d-]*)\/([\d-]*)",$linearray[10]);
		$this->setStatusCode($linearray[11]);
		$this->setBytesRead($linearray[12]);
		$this->setCapturedRequestCookie($linearray[13]);
		$this->setCapturedResponseCookie($linearray[14]);
		$this->setTerminationState($linearray[15]);
		list(,$this->actconn,$this->feconn,$this->beconn,$this->srv_conn,$this->retries)=RegexMatch::getMatches("([\d-])*\/([\d-]*)\/([\d-]*)\/([\d-]*)\/([\d-]*)",$linearray[16]);
		list(,$this->srv_queue,$this->backend_queue)=RegexMatch::getMatches("([\d-])*\/([\d-]*)",$linearray[17]);


		$linecount=count($linearray);	
	 	if($linecount>21) {
			list(,$this->captured_request_headers)=RegexMatch::getMatches("{(.*)}",$linearray[18]);		
			list(,$this->captured_response_headers)=RegexMatch::getMatches("{(.*)}",$linearray[19]);		
		}
		
		$this->setHttpRequest($linearray[$linecount-3].$linearray[$linecount-2].$linearray[$linecount-1]);

		return $this;	
	}

	public function getAcceptDate(){
		return ($this->accept_date);
	}	

	public function getTr(){
                return ($this->tr);
        }

	public function setProcessName($process_name){
		$this->process_name=$process_name;
	}
	
	public function setPid($pid){
		$this->pid=$pid;
	}
	
	public function setClientIp($client_ip){
		$this->client_ip=$client_ip;
	}
	
	public function setClientPort($client_port){
		$this->client_port=$client_port;
	}
	
	public function setAcceptDate($accept_date){
		$this->accept_date=$accept_date;
	}
	
	public function setFrontendName($frontend_name){
		$this->frontend_name=$frontend_name;
	}
	
	public function setBackendName($backend_name){
		$this->backend_name=$backend_name;
	}
	
	public function setServerName($server_name){
		$this->server_name=$server_name;
	}
	
	public function setTq($tq){
		$this->tq=$tq;
	}
	
	public function setTw($tw){
		$this->tw=$tw;
	}
	
	public function setTc($tc){
		$this->tc=$tc;
	}
	
	public function setTr($tr){
		$this->tr=$tr;
	}
	
	public function setTt($tt){
		$this->tt=$tt;
	}
	
	public function setStatusCode($status_code){
		$this->status_code=$status_code;
	}
	
	public function setBytesRead($bytes_read){
		$this->bytes_read=$bytes_read;
	}
	
	public function setCapturedRequestCookie($captured_request_cookie){
		$this->captured_request_cookie=$captured_request_cookie;
	}
	
	public function setCapturedResponseCookie($captured_response_cookie){
		$this->captured_response_cookie=$captured_response_cookie;
	}
	
	public function setTerminationState($termination_state){
		$this->termination_state=$termination_state;
	}
	
	public function setActconn($actconn){
		$this->actconn=$actconn;
	}
	
	public function setFeconn($feconn){
		$this->feconn=$feconn;
	}
	
	public function setBeconn($beconn){
		$this->beconn=$beconn;
	}
	
	public function setSrvConn($srv_conn){
		$this->srv_conn=$srv_conn;
	}
	
	public function setRetries($retries){
		$this->retries=$retries;
	}
	
	public function setSrvQueue($srv_queue){
		$this->srv_queue=$srv_queue;
	}
	
	public function setBackendQueue($backend_queue){
		$this->backend_queue=$backend_queue;
	}

	public function setCapturedRequestHeaders($captured_request_headers){
		$this->captured_request_headers=$captured_request_headers;
	}
	
	public function setCapturedResponseHeaders($captured_response_headers){
		$this->captured_response_headers=$captured_response_headers;
	}
	
	public function setHttpRequest($http_request){
		$this->http_request=$http_request;
	}

}
?>
