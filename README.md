# README #

### Haproxy log analyzer API ###

Rest API to extract and process information from Haproxy load balancer logs.
Returns output as json. This API is exposed to clients to get desired information from the log.

### File Layout ###

* Sample Haproxy log provided as input is in **data** directory.
* REST API exposed to clients is in **api** directory. The API generates output formatted as a JSON.
* Files performing extraction and processing of log data are in **classes** directory.

### Contribution guidelines ###

* Writing tests
* Code review

### Who do I talk to? ###

* Vinay (vinayb211@gmail.com)