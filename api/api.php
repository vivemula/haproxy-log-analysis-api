<?php
//REST API to return response time from Haproxy log as json 
//This API is exposed to clients to get desired information from the log.
//Author: Vinay

require_once("settings.php");
require_once(WEB_ROOT."/classes/Main.php");
require_once(WEB_ROOT."/classes/Log.php");

$json=array();
$json['error']='false';
$json['starttime']='';
$json['duration']='';

$json['createDate']=date("Y-m-d H:i:s");

if(!isset($_GET) || !isset($_GET['starttime']) || !isset($_GET['ver'])|| !isset($_GET['format'])){
        $json['error']='true';
	$json['errorMessage']='No parameters in the request';
}
else {
	//Parameters
	$starttime=$_GET['starttime'];
	$duration=$_GET['duration'];
	$version=$_GET['ver'];	
	$format=$_GET['format'];

	$json['version']=$version;
	$json['starttime']=$starttime;
	$json['duration']=$duration;

	if($version!=1){
		$json['error']='true';
		$json['errorMessage']='Invalid API version';
	}else if($format!='json'){
		$json['error']='true';
		$json['errorMessage']='Unsupported output format';
	}else{
		//Valid output with response indicating success
		$headerSet=true;
		header('HTTP/1.1 200 OK');
		header('Content-type: application/json');

		//Create Log objects once
		if(Main::getLogObjSize()==0)
			$MainObj=new Main();
		
		$LogObj=$MainObj->getLogObj($starttime);

		if($LogObj==null){
			$json['error']='true';	
			$json['errorMessage']='No log found for this starttime';
		}else{
			$roundtripTime=$LogObj->getTr();
			if($roundtripTime==-1) {
				$json['error']='true';
				$json['errorMessage']='Incomplete response or connection closed';
			}else
				$json['roundtripTime']=$roundtripTime;
		}
	}
}

//Send response to client indicating error
if(!$headerSet)
	header('HTTP/1.1 400 BAD REQUEST');

echo json_encode($json);
